//
//  AppDelegate.h
//  KaizoVisualNovel
//
//  Created by Nguyen Viet Duc on 16.04.17.
//  Copyright © 2017 Nguyen Viet Duc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VNSystemCall.h"
#import "VNScene.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
	VNSystemCall* systemCallHelper;
	BOOL muted;
}


@property (strong, nonatomic) UIWindow *window;



@end

