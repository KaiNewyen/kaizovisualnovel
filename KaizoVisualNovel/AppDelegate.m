//
//  AppDelegate.m
//  KaizoVisualNovel
//
//  Created by Nguyen Viet Duc on 16.04.17.
//  Copyright © 2017 Nguyen Viet Duc. All rights reserved.
//

#import "AppDelegate.h"
#import "VNSystemCall.h"
#import "VNScene.h"


@interface AppDelegate ()

@end

@implementation AppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	systemCallHelper = [[VNSystemCall alloc] init];
	[systemCallHelper autosave];
	NSLog(@"[appDelegae] inactive state phone call for instance");
}

- (void)applicationDidEnterBackground:(UIApplication *)application {

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    systemCallHelper = [[VNSystemCall alloc] init];
    if ([systemCallHelper isMute]){
    	muted = YES;
    }
    else{
		[systemCallHelper mute];
		muted = NO;
	}
	NSLog(@"[appDelegae] Enter Background");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	systemCallHelper = [[VNSystemCall alloc] init];
	[systemCallHelper autosave];
	NSLog(@"[appDelegae] Background to inactive");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	if(muted){}
	else{
		[systemCallHelper unmute];
	}

	NSLog(@"[appDelegae] Become Active again");
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	systemCallHelper = [[VNSystemCall alloc] init];
	[systemCallHelper autosave];
	NSLog(@"[appDelegae] terminate app");
}

@end
